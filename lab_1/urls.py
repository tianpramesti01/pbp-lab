from django.urls import path
from .views import index, friend_list

urlpatterns = [
    # TODO Add friends path using friend_list Views
    path('', index, name='index'),
    path('friends', friend_list)
]
