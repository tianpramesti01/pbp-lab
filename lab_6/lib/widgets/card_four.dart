import 'package:flutter/material.dart';

class CardFour extends StatefulWidget {
  const CardFour({Key? key}) : super(key: key);

  @override
  _CardFourState createState() => _CardFourState();
}

class _CardFourState extends State<CardFour> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        child: InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            print('Card tapped.');
          },
          child: SizedBox(
            width: 300,
            // height: 200,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    children: [
                      Image.asset('assets/images/distancing.jpg'),
                      const SizedBox(height: 10.0),
                      const Text(
                        "Ini 5 Cara Melakukan Physical Distancing",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      const Text(
                        "Menerapkan physical distancing mampu mencegah penyebaran virus corona semakin banyak.",
                        style: TextStyle(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                      SizedBox(height: 12),
                      ElevatedButton(onPressed: () {}, child: Text("Read More"))
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
