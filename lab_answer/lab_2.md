# Lab2 Answers

## Perbedaan XML dan JSON

|Parameter         |XML         |JSON         |
| ----------- | ----------- | ----------- |
| Bahasa | XML adalah singkatan dari eXtensible Mark-up Language | JSON adalah singkatan dari JavaScript Object Notation. |
| Kecepatan | Lebih lambat dalam transmisi data | Lebih cepat karena ukuran file kecil |
| Dukungan Namespaces | Mendukung namespaces, komentar dan metadata | Tidak mendukung namespaces, komentar, dan metadata |
| Orientasi | XML document-oriented | JSON data-oriented |
| Tipe Data | Mendukung banyak tipe data kompleks termasuk bagan, charts, dan tipe data non-primitif lainnya | Tipe data skalar, data struktur dapat diekspresikan menggunakan array dan objek |
| Dukungan Array | Tidak mendukung array secara langsung | Mendukung array yang dapat diakses |
| Keamanan | Lebih aman | Kurang aman |

<br>

## Perbedaan XML dan HTML

|Parameter         |XML         |HTML         |
| ----------- | ----------- | ----------- |
| Tipe bahasa | Case sensitive | Bukan case sensitive |
| Tujuan | Transfer data | Penyajian data |
| Kesalahan Pengkodean | Tidak diperbolehkan | Kesalahan minor diabaikan |
| White Spaces | Bisa digunakan | Tidak bisa digunakan |
| End of tags | Beberapa tag tidak memerlukan tag penutup | Setiap tag membutuhkan tag penutup |
| Ukuran Dokumen | Lebih besar | Lebih kecil |

<br>

## Referensi:
https://www.guru99.com/xml-vs-html-difference.html#7 <br>
https://www.javatpoint.com/html-vs-xml <br>
https://www.w3schools.com/js/js_json_xml.asp  <br>
https://askanydifference.com/difference-between-json-and-xml/ <br>
https://www.javatpoint.com/json-vs-xml
