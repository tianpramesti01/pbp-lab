import 'package:flutter/material.dart';
import 'package:lab_7/widgets/buat_artikel.dart';
import 'package:lab_7/widgets/card_one.dart';
import 'package:lab_7/widgets/card_two.dart';
import 'package:lab_7/widgets/card_three.dart';
import 'package:lab_7/widgets/card_four.dart';
import 'package:lab_7/widgets/carousel.dart';
import 'package:lab_7/widgets/carousel_two.dart';

class EdukasiProtokol extends StatefulWidget {
  const EdukasiProtokol({Key? key}) : super(key: key);

  @override
  _EdukasiProtokolState createState() => _EdukasiProtokolState();
}

class _EdukasiProtokolState extends State<EdukasiProtokol> {
  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Carousel(),
      SizedBox(height: 18),
      Padding(
        padding: EdgeInsets.all(16.0),
        child: Text(
          "Edukasi Protokol COVID-19",
          textAlign: TextAlign.center,
          style: TextStyle(
              fontWeight: FontWeight.w500, fontSize: 32, color: Colors.black),
        ),
      ),
      SizedBox(height: 18),
      CardOne(),
      SizedBox(height: 18),
      CardTwo(),
      SizedBox(height: 18),
      CardThree(),
      SizedBox(height: 18),
      CardFour(),
      SizedBox(height: 12),
      Row(mainAxisAlignment: MainAxisAlignment.center, children: [
        ElevatedButton(onPressed: () {}, child: Text("Load More")),
      ]),
      SizedBox(height: 50),
      CarouselTwo(),
      SizedBox(height: 50),
      BuatArtikel()
    ]);
  }
}
