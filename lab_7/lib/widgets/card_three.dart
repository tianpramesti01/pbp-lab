import 'package:flutter/material.dart';

class CardThree extends StatefulWidget {
  const CardThree({Key? key}) : super(key: key);

  @override
  _CardThreeState createState() => _CardThreeState();
}

class _CardThreeState extends State<CardThree> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        child: InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            print('Card tapped.');
          },
          child: SizedBox(
            width: 300,
            // height: 200,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    children: [
                      Image.asset('assets/images/cucimasker.jpg'),
                      const SizedBox(height: 10.0),
                      const Text(
                        "Cara Mencuci Masker Kain Anjuran Kemenkes",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      const Text(
                        "Masker kain harus dicuci dengan cara yang tepat seperti anjuran dari Kementerian Kesehatan Republik Indonesia (Kemenkes RI).",
                        style: TextStyle(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                      SizedBox(height: 12),
                      ElevatedButton(onPressed: () {}, child: Text("Read More"))
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
