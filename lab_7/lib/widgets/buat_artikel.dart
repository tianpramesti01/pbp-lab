import 'package:flutter/material.dart';
import 'package:lab_7/screen/add_artikel.dart';

class BuatArtikel extends StatelessWidget {
  const BuatArtikel({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Stack(children: [
      Image.asset('assets/images/pict.jpg'),
      Positioned(
        child: SizedBox(
          width: 300,
          child: Column(children: [
            Text("Hello, Tian!",
                style: TextStyle(
                  fontSize: 18,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                )),
            Text(
              "Anda dapat menambahkankan artikel terkait edukasi protokol COVID-19 di sini!",
              style: TextStyle(
                fontSize: 11,
                color: Colors.white,
                fontWeight: FontWeight.normal,
              ),
              textAlign: TextAlign.center,
            ),
            Text(
              "Bagikan kebaikan, bagikan informasi kepada sesama",
              style: TextStyle(
                fontSize: 9,
                color: Colors.white,
                fontWeight: FontWeight.normal,
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 12),
            // ElevatedButton(onPressed: () {}, child: Text("Buat Artikel"))
            Padding(
              padding: const EdgeInsets.only(top: 12.0),
              child: TextButton(
                child: const Text(
                  "Buat Artikel",
                  style: TextStyle(color: Colors.white),
                ),
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(const Color(0xFF0B36A8)),
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => AddArtikel(),
                    ),
                  );
                },
              ),
            ),
          ]),
        ),
        left: 30,
        top: 40,
      ),
    ]));
  }
}
