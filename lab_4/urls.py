from django.urls.conf import path
from lab_4.views import index, add_note, note_list

urlpatterns = [
    path('', index, name='index_lab4'),
    path('add-note', add_note, name='add_note'),
    path('note-list', note_list, name='note-list'),
]