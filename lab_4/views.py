from django.shortcuts import render
from lab_2.models import Note
from lab_4.forms import NoteForm
from django.http.response import HttpResponse, HttpResponseRedirect

def index(request):
    data = Note.objects.all()
    response = {'data':data}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    if request.method == 'POST':
        form = NoteForm(request.POST)
        if form.is_valid():
            form.save()  # Save data to DB
            return HttpResponseRedirect('/lab-4/')  # Redirect on finish

    # if a GET (or any other method) we'll create a blank form
    else:
        form = NoteForm()

    return render(request, 'lab4_form.html', {'form': form})

def note_list(request):
    data = Note.objects.all()
    response = {'data':data}
    return render(request, 'lab4_note_list.html', response)