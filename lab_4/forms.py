from lab_2.models import Note
from django import forms
from django.forms import ModelForm

class NoteForm(ModelForm):
    class Meta:
        model = Note
        fields = "__all__"