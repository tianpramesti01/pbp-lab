from django.shortcuts import render
from lab_1.models import Friend
from .forms import FriendForm
from django.http.response import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url="/admin/login/")
def index(request):
    friends = Friend.objects.all() # TODO Implement this
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url="/admin/login/")
def add_friend(request):
    if request.method == 'POST':
        form = FriendForm(request.POST)
        if form.is_valid():
            form.save()  # Save data to DB
            return HttpResponseRedirect('/lab-3/')  # Redirect on finish

    # if a GET (or any other method) we'll create a blank form
    else:
        form = FriendForm()

    return render(request, 'lab3_form.html', {'form': form})