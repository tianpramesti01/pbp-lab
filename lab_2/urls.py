from django.urls.conf import path
from lab_2.views import index, json, xml

urlpatterns = [
    path('', index, name='index'),
    path('xml', xml),
    path('json', json)
]