from django.db import models

# Create your models here.
class Note(models.Model):
    # TODO Implement missing attributes in Friend model
    To = models.CharField(max_length=300)
    From = models.CharField(max_length=300)
    Title = models.CharField(max_length=300)
    Message = models.TextField()

    def __str__(self):
        return self.To